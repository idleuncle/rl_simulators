# Kaggle Environments

https://github.com/Kaggle/kaggle-environments/

> Kaggle Environments was created to evaluate episodes. While other libraries have set interface precedents (such as Open.ai Gym), the emphasis of this library focuses on:
>
> - Episode evaluation (compared to training agents).
> - Configurable environment/agent lifecycles.
> - Simplified agent and environment creation.
> - Cross language compatible/transpilable syntax/interfaces.

```bash
pip install kaggle-environments
```

## Kore 2022

https://www.kaggle.com/competitions/kore-2022-beta

